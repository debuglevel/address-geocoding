package de.debuglevel.addressgeocoding.geocoder

/**
 * Base class for a specialized [GeocoderProperties] for a certain [Geocoder]; usually constructed based on a [GeocoderConfiguration].
 */
open class GeocoderProperties {
    /**
     * Whether the Geocoder is enabled
     * TODO: does not seem to be respected anywhere. Maybe do not create bean in GeocoderFactory?
     */
    var enabled: Boolean = false

    /**
     * Maximum parallel geocoding requests.
     */
    var maximumThreads: Int = 1

    /**
     * How long should be waited between two requests (in nanoseconds)
     */
    var waitBetweenRequests: Long = 1_000_000_000

    /**
     * Base URL of the service
     */
    var url: String = "invalid"

    override fun toString(): String {
        return "GeocoderProperties(" +
                "enabled=$enabled, " +
                "maximumThreads=$maximumThreads, " +
                "waitBetweenRequests=$waitBetweenRequests, " +
                "url='$url'" +
                ")"
    }
}